/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.ac.unpar.informatika.kamin.minggu01;

import java.math.BigInteger;
import java.util.Scanner;

/**
 * @author G. Christianto 2016730011
 * @author Jason Yehezkiel 2016730087
 */
public class Pejalan {

    public static void main(String[] args) {
        /// get p and gs.
        //initialization
        Scanner sc = new Scanner(System.in);

        // TANYA P
        System.out.println("[SYSTEM] Masukan bilangan prima p atau 'gen' untuk membangkitkan bilangan p: ");
        String p = sc.nextLine().replace("\n", "");
        BigInteger theP = null;

        // DF Initialization
        // Kita menggunakan 2 buah kelas yang berbeda untuk
        // mensimulasi percakapan.
        id.ac.unpar.informatika.kamin.minggu01.Tugas.DiffieHelman dfAlice = new id.ac.unpar.informatika.kamin.minggu01.Tugas.DiffieHelman();
        DiffieHelman dfBob = DiffieHelman.getInstance();

        if (p.equalsIgnoreCase("gen")) {
            dfAlice.setP(null);
            theP = dfAlice.getTheP();
            dfBob.setP(theP.toString());
        } else {
            theP = new BigInteger(p);
            dfAlice.setP(theP);
            dfBob.setP(p);
        }

        System.out.println("[SYSTEM] p is set into " + theP.toString() + ". Applied to Bob and Alice.");

        // ============================
        System.out.println("[SYSTEM] Massukan g (2 <= g <= p-1): ");
        p = sc.nextLine().replace("\n", "");
        BigInteger theG = null;
        if (p.equalsIgnoreCase("gen")) {
            dfAlice.setG(null);
            theG = dfAlice.getTheG();
            dfBob.setG(theG);
        } else {
            theG = new BigInteger(p);
            dfAlice.setG(theG);
            dfBob.setG(theG);
        }
        dfBob.generatePrivateKey();
        System.out.println("[SYSTEM] g is set into " + theG.toString() + ". Applied to Bob and Alice.");

        System.out.println("[ALICE-] Alice public key (A) is " + dfAlice.getPublic());
        System.out.println("[SYSTEM] A will be sent to Bob.");
        System.out.println("[BOB---] Bob recieved A. Bob calculates shared secret...");
        BigInteger bobSharedSecret = dfBob.calculateS(dfAlice.getPublic());
        System.out.println("[BOB---] Bob finished calculated the S. Result: " + bobSharedSecret);
        System.out.println("[BOB---] Bob public key (B) is " + dfBob.calculatePublicA());
        System.out.println("[SYSTEM] B will be sent to Alice.");
        System.out.println("[ALICE-] Alice recieved B. Alice calucates shared secret...");
        BigInteger aliceSharedSecret = dfAlice.calcSharedSecret(dfBob.calculatePublicA());
        System.out.println("[ALICE-] Alice finished calculated the S. Result: " + aliceSharedSecret);
        boolean isSharedSecretSame = aliceSharedSecret.equals(bobSharedSecret);
        System.out.println("[------] As per our conclusion, the shared secret that both recieved are " + (isSharedSecretSame ? "matching" : "not matching") + " each other.");
        sc.close();
    }

}
