/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.ac.unpar.informatika.kamin.minggu01.Tugas;

import java.math.BigInteger;
import java.security.InvalidParameterException;
import java.security.SecureRandom;

/**
 *
 * @author hayashi
 */
public class DiffieHelman {
    // ini var p
    protected BigInteger thePrivate = null;
    
    // PUBLIC SECTION
    
    // ini var p
    protected BigInteger theP = null;    
    // ini buat yang nanti datanya.
    protected BigInteger theG = null;
    
    public static int bitLength = 64;
    
    public DiffieHelman () {
        this.setPrivate(null);
    }
    
    public void setPrivate(BigInteger privateKey) {
        if (privateKey == null) {
            this.thePrivate = BigInteger.probablePrime(DiffieHelman.bitLength, new SecureRandom());
        } else {
            this.thePrivate = privateKey;
        }
    }
    
    public void setP(BigInteger privateKey) {
        if (privateKey == null) {
            this.theP = BigInteger.probablePrime(DiffieHelman.bitLength, new SecureRandom());
        } else {
            this.theP = privateKey;
        }
    }
    public void setG(BigInteger g) {
        if (g == null) {
            this.theG = BigInteger.probablePrime(DiffieHelman.bitLength, new SecureRandom());
        } else {
            if(g.compareTo(BigInteger.TWO) >= 0 && g.compareTo(this.getTheP().subtract(BigInteger.ONE)) <= 0) {
                this.theG = g;
            } else {
                throw new InvalidParameterException("The G should satisfy  2 <= g <= p-1.");
            }
        }
    }
    
    public BigInteger getPublic () {
        return this.calcPublic(null);
    }
    
    public BigInteger calcSharedSecret(BigInteger otherPublic) {
        return this.calcPublic(otherPublic);
    }
    
    public BigInteger calcPublic(BigInteger otherPublic) {
        if (otherPublic == null) {
            return this.getTheG().modPow(this.getThePrivate(), getTheP());
        } else {
            return otherPublic.modPow(this.getThePrivate(), getTheP());
        }
    }

    /**
     * @return the thePrivate
     */
    public BigInteger getThePrivate() {
        return thePrivate;
    }

    /**
     * @return the theP
     */
    public BigInteger getTheP() {
        return theP;
    }

    /**
     * @return the theG
     */
    public BigInteger getTheG() {
        return theG;
    }
}
