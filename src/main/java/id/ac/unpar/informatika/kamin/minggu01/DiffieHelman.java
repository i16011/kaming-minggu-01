package id.ac.unpar.informatika.kamin.minggu01;

import java.math.BigInteger;
import java.util.Random;

public class DiffieHelman{

    private static DiffieHelman instance;
    private BigInteger primeSeed;
    private BigInteger g;
    private BigInteger privateA;
    private BigInteger publicA;

    public static DiffieHelman getInstance(){
        if(instance == null){
            instance = new DiffieHelman();
        }
        return instance;
    }

    public BigInteger generateRandomPublicKey() {
        Random rnd = new Random();
        this.primeSeed = BigInteger.probablePrime(64, rnd);
        return this.primeSeed;
    }

    public void setP(String p) {
        this.primeSeed = new BigInteger(p);
    }

    public BigInteger generatePrivateKey() {
        Random rnd = new Random();
        this.privateA = BigInteger.probablePrime(64, rnd);
        return this.privateA;
    }

    public BigInteger calculatePublicA() {
        this.publicA = this.g.modPow(this.privateA, this.primeSeed);
        return this.publicA;
    }

    public void setG(BigInteger g) {
        this.g = g;
    }

    public BigInteger calculateS(BigInteger b) {
        return b.modPow(this.privateA, this.primeSeed);
    }
}